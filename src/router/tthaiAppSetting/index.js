export default {
  path: '/eats-zaab',
  component: () => import('@/views/tthaiAppSetting/index.vue'),
  children: [
    {
      path: '',
      name: 'TThaiAppPage',
      component: () => import('@/views/tthaiAppSetting/page/TthaiAppSettingPage.vue'),
      meta: {
        appBar: {
          title: 'EatsZaab Setting',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-star-shooting' }
        }
      }
    }

  ]
}
