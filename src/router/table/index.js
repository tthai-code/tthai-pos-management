export default {
  path: '/table',
  component: () => import('@/views/table/index.vue'),
  children: [
    {
      path: '',
      name: 'TableList',
      component: () => import('@/views/table/page/TableList.vue'),
      meta: {
        appBar: {
          title: 'Table',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-stool' }
        }
      }
    },
    {
      path: 'add',
      name: 'TableAdd',
      component: () => import('@/views/table/page/TableAdd.vue'),
      meta: {
        appBar: {
          title: 'Table',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-stool' }
        }
      }
    },
    {
      path: 'add/:tableId',
      name: 'TableEdit',
      component: () => import('@/views/table/page/TableAdd.vue'),
      meta: {
        appBar: {
          title: 'Table',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-stool' }
        }
      }
    }
    // {
    //   path: 'attribute',
    //   name: 'TableAttribute',
    //   component: () => import('@/views/table/page/TableZoneList.vue'),
    //   meta: {
    //     appBar: {
    //       title: 'Table Attribute Setting',
    //       // FIXME: mdi-stool
    //       icon: { icon: 'mdi-stool' }
    //     }
    //   }
    // },
    // {
    //   path: 'attribute/add',
    //   name: 'TableAttributeAdd',
    //   component: () => import('@/views/tableZone/pages/TableAttributeAdd.vue'),
    //   meta: {
    //     appBar: {
    //       title: 'Table Attribute Setting',
    //       // FIXME: mdi-stool
    //       icon: { icon: 'mdi-stool' }
    //     }
    //   }
    // },
    // {
    //   path: 'attribute/add/:seatTypeId',
    //   name: 'TableAttributeEdit',
    //   component: () => import('@/views/tableZone/pages/TableAttributeAdd.vue'),
    //   meta: {
    //     appBar: {
    //       title: 'Table Attribute Setting',
    //       // FIXME: mdi-stool
    //       icon: { icon: 'mdi-stool' }
    //     }
    //   }
    // }
  ]
}
