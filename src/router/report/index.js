export default {
  path: '/dashboard',
  component: () => import('@/views/report/index.vue'),
  children: [
    {
      path: '',
      name: 'ReportHome',
      component: () => import('@/views/report/page/ReportHome.vue'),
      meta: {
        appBar: {
          title: 'Sales Report',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-chart-pie' }
        }
      }
    }

  ]
}
