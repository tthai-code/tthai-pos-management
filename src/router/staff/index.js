export default {
  path: '/staff',
  component: () => import('@/views/staff/index.vue'),
  children: [
    {
      path: '',
      name: 'StaffList',
      component: () => import('@/views/staff/page/StaffList.vue'),
      meta: {
        appBar: {
          title: 'Staff',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-account-circle-outline' }
        }
      }
    },
    {
      path: 'add',
      name: 'StaffAdd',
      component: () => import('@/views/staff/page/StaffAdd.vue'),
      meta: {
        appBar: {
          title: 'Staff',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-account-circle' }
        }
      }
    },
    {
      path: 'add/:staffId',
      name: 'StaffEdit',
      component: () => import('@/views/staff/page/StaffAdd.vue'),
      meta: {
        appBar: {
          title: 'Staff',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-account-circle' }
        }
      }
    }
  ]
}
