export default {
  path: '/device',
  component: () => import('@/views/device/index.vue'),
  children: [
    {
      path: '',
      name: 'DeviceList',
      component: () => import('@/views/device/page/DeviceList.vue'),
      meta: {
        appBar: {
          title: 'Device',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-devices' }
        }
      }
    }

  ]
}
