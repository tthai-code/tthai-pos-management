export default {
  path: '/restaurant',
  component: () => import('@/views/restaurant/index.vue'),
  children: [
    {
      path: 'setting',
      name: 'MenuList',
      component: () => import('@/views/restaurant/pages/RestaurantSetting.vue'),
      meta: {
        appBar: {
          title: 'Restaurant Setting',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-domain' }
        }
      }
    }

  ]
}
