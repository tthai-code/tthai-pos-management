export default {
  path: '/modify',
  component: () => import('@/views/modify/index.vue'),
  children: [
    {
      path: '',
      name: 'ModifyList',
      component: () => import('@/views/modify/page/ModifyList.vue'),
      meta: {
        appBar: {
          title: 'Menu Modifier',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-apple-keyboard-option' }
        }
      }
    },
    {
      path: 'add',
      name: 'ModifyAdd',
      component: () => import('@/views/modify/page/ModifyAdd.vue'),
      meta: {
        appBar: {
          title: 'Menu Modifier',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-apple-keyboard-option' }
        }
      }
    },
    {
      path: 'add/:modifierId',
      name: 'ModifyEdit',
      component: () => import('@/views/modify/page/ModifyAdd.vue'),
      meta: {
        appBar: {
          title: 'Menu Modifier',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-apple-keyboard-option' }
        }
      }
    }

  ]
}
