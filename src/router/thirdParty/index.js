export default {
  path: '/delivery',
  component: () => import('@/views/third-party/index.vue'),
  children: [
    {
      path: '',
      name: 'ThirdParty',
      component: () => import('@/views/third-party/page/ThirdParty.vue'),
      meta: {
        appBar: {
          title: 'Delivery',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-delivery' }
        }
      }
    }

  ]
}
