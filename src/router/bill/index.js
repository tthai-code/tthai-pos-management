export default {
  path: '/bill',
  component: () => import('@/views/bill/index.vue'),
  children: [
    {
      path: '',
      name: 'BillList',
      component: () => import('@/views/bill/pages/BillList.vue'),
      meta: {
        appBar: {
          title: 'Bill',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-receipt' }
        }
      }
    }

  ]
}
