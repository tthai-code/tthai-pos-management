export default {
  path: '/tax',
  component: () => import('@/views/tax/index.vue'),
  children: [
    {
      path: '',
      name: 'TaxSetting',
      component: () => import('@/views/tax/pages/TaxList.vue'),
      meta: {
        appBar: {
          title: 'Tax',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-ticket-percent' }
        }
      }
    }

  ]
}
