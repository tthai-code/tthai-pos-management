export default {
  path: '/menu-category',
  component: () => import('@/views/menu/index.vue'),
  children: [
    {
      path: '',
      name: 'MenuCategory',
      component: () => import('@/views/menu/page/MenuAttribute.vue'),
      meta: {
        appBar: {
          title: 'Menu Category',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-stool' }
        }
      }
    },
    {
      path: 'add',
      name: 'MenuAttributeAdd',
      component: () => import('@/views/menu/page/MenuAttributeAdd.vue'),
      meta: {
        appBar: {
          title: 'Menu Category',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-stool' }
        }
      }
    },
    {
      path: 'edit/:menuTypeId',
      name: 'MenuAttributeEdit',
      component: () => import('@/views/menu/page/MenuAttributeAdd.vue'),
      meta: {
        appBar: {
          title: 'Menu Category',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-stool' }
        }
      }
    },
    {
      path: 'edit-position',
      name: 'MenuPositionPage',
      component: () => import('@/views/menu/page/MenuAttributePosition.vue'),
      meta: {
        appBar: {
          title: 'Menu Category',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-stool' }
        }
      }
    }
  ]
}
