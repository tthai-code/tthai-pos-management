export default {

  path: '/transaction',
  component: () => import('@/views/catering/index.vue'),
  children: [
    {
      path: '',
      name: 'transactionsPage',
      component: () => import('@/views/transactions/page/TransactionsPage.vue'),
      meta: {
        appBar: {
          title: 'Transactions',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-swap-horizontal' }
        }
      }
    }

  ]
}
