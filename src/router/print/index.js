export default {
  path: '/print',
  component: () => import('@/views/print/index.vue'),
  children: [
    {
      path: 'receipt/:billingId',
      name: 'print',
      component: () => import('@/views/print/pages/Receipt.vue'),
      meta: {
        layout: 'blank'
      }
    }

  ]
}
