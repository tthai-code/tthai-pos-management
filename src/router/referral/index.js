export default {
  path: '/referral',
  component: () => import('@/views/referral/index.vue'),
  children: [
    {
      path: '',
      name: 'ReferralList',
      component: () => import('@/views/referral/pages/ReferralList.vue'),
      meta: {
        appBar: {
          title: 'Referral',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-code-equal' }
        }
      }
    }

  ]
}
