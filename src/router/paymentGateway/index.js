export default {
  path: '/payment',
  component: () => import('@/views/payment-gateway/index.vue'),
  children: [
    {
      path: '',
      name: 'PaymentGateWay',
      component: () => import('@/views/payment-gateway/page/PaymentGateway.vue'),
      meta: {
        appBar: {
          title: 'Payment Gateway',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-account-credit-card-outline' }
        }
      }
    }

  ]
}
