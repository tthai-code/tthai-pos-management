export default {
  path: '/table-zone',
  component: () => import('@/views/tableZone/index.vue'),
  children: [
    {
      path: '',
      name: 'TableList',
      component: () => import('@/views/tableZone/pages/TableZoneList.vue'),
      meta: {
        appBar: {
          title: 'TableZone',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-stool' }
        }
      }
    },
    {
      path: 'add',
      name: 'TableZoneAdd',
      component: () => import('@/views/tableZone/pages/TableZoneAdd.vue'),
      meta: {
        appBar: {
          title: 'Table',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-stool' }
        }
      }
    },
    {
      path: 'add/:zoneId',
      name: 'TableEdit',
      component: () => import('@/views/tableZone/pages/TableZoneAdd.vue'),
      meta: {
        appBar: {
          title: 'Table',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-stool' }
        }
      }
    }
    // {
    //   path: 'attribute',
    //   name: 'TableAttribute',
    //   component: () => import('@/views/table/page/TableZoneList.vue'),
    //   meta: {
    //     appBar: {
    //       title: 'Table Attribute Setting',
    //       // FIXME: mdi-stool
    //       icon: { icon: 'mdi-stool' }
    //     }
    //   }
    // },
    // {
    //   path: 'attribute/add',
    //   name: 'TableAttributeAdd',
    //   component: () => import('@/views/table/page/TableAttributeAdd.vue'),
    //   meta: {
    //     appBar: {
    //       title: 'Table Attribute Setting',
    //       // FIXME: mdi-stool
    //       icon: { icon: 'mdi-stool' }
    //     }
    //   }
    // },
    // {
    //   path: 'attribute/add/:seatTypeId',
    //   name: 'TableAttributeEdit',
    //   component: () => import('@/views/table/page/TableAttributeAdd.vue'),
    //   meta: {
    //     appBar: {
    //       title: 'Table Attribute Setting',
    //       // FIXME: mdi-stool
    //       icon: { icon: 'mdi-stool' }
    //     }
    //   }
    // }
  ]
}
