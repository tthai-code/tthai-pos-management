export default {
  path: '/menu',
  component: () => import('@/views/menu/index.vue'),
  children: [
    {
      path: '',
      name: 'MenuList',
      component: () => import('@/views/menu/page/MenuList.vue'),
      meta: {
        appBar: {
          title: 'Menu',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-stool' }
        }
      }
    },
    {
      path: 'add',
      name: 'MenuAdd',
      component: () => import('@/views/menu/page/MenuAdd.vue'),
      meta: {
        appBar: {
          title: 'Menu',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-stool' }
        }
      }
    },
    {
      path: 'add/:menuId',
      name: 'MenuEdit',
      component: () => import('@/views/menu/page/MenuAdd.vue'),
      meta: {
        appBar: {
          title: 'Menu',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-stool' }
        }
      }
    },
    {
      path: 'edit-position',
      name: 'MenuPositionPage',
      component: () => import('@/views/menu/page/MenuPosition.vue'),
      meta: {
        appBar: {
          title: 'Menu Category',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-stool' }
        }
      }
    }
  ]
}
