export default {
  path: '/reservation',
  component: () => import('@/views/reservation/index.vue'),
  children: [
    {
      path: '',
      name: 'Reservation',
      component: () => import('@/views/reservation/page/ReservationPage.vue'),
      meta: {
        appBar: {
          title: 'Reservation',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-timetable' }
        }
      }
    }

  ]
}
