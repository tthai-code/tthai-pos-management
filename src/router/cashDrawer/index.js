export default {
  path: '/cash-drawer',
  component: () => import('@/views/cash-drawer/index.vue'),
  children: [
    {
      path: '',
      name: 'CashDrawer',
      component: () => import('@/views/cash-drawer/pages/CashDrawerList.vue'),
      meta: {
        appBar: {
          title: 'Cash Drawer',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-cash-fast' }
        }
      }
    },
    {
      path: ':cashDrawerId/activity',
      name: 'CashDrawer',
      component: () => import('@/views/cash-drawer/pages/CashDrawerActivity.vue'),
      meta: {
        appBar: {
          title: 'Cash Drawer',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-cash-fast' }
        }
      }
    }
  ]
}
