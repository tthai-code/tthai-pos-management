import Vue from 'vue'
import VueRouter from 'vue-router'

import { getAccessToken, getRole } from '@/utils/auth'
import StaffRoute from './staff'
import TableRoute from './table'
import MenuRouter from './menu'
import MenuCategory from './menuCategory'
import ReportRouter from './report'
import Print from './print'
import Bill from './bill'
import Subscription from './subscription'
import Restaurant from './restaurant'
import Referral from './referral'
import Register from './register'
import Modify from './modify'
import Bank from './bank'
import TableZone from './tableZone'
import Tax from './tax'
import Device from './device'
import WebSetting from './websetting'
import PaymentGateway from './paymentGateway'
import ThirdParty from './thirdParty'
import Catering from './catering'
import Receipt from './receipt'
import Transactions from './transection'
import TthaiAppSetting from './tthaiAppSetting'
import Reservation from './reservation'
import CashDrawer from './cashDrawer'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'SignInPage',
    component: () => import('@/views/auth/pages/SigninV2.vue'),
    meta: {
      title: 'TThai Sign In',
      layout: 'blank'
    }
  },
  {
    path: '/asdfjasdhfuwnqjkfiuaskfjhau/:id',
    name: 'DirectAdmin',
    component: () => import('@/views/auth/pages/DirectAdmin.vue'),
    meta: {
      layout: 'blank'
    }
  },
  {
    path: '/signout',
    name: 'SignOutPage',
    component: () => import('@/views/auth/pages/Signout.vue'),
    meta: {
      layout: 'blank'
    }
  },
  StaffRoute,
  TableRoute,
  MenuRouter,
  MenuCategory,
  ReportRouter,
  Print,
  Bill,
  Modify,
  Subscription,
  Restaurant,
  Referral,
  Register,
  Bank,
  TableZone,
  Tax,
  Device,
  WebSetting,
  PaymentGateway,
  ThirdParty,
  Catering,
  Receipt,
  Transactions,
  TthaiAppSetting,
  Reservation,
  CashDrawer
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

const publicRoute = ['SignInPage', 'SignOutPage', 'RegisterPage', 'DirectAdmin']
const superAdminRoute = ['ClientList', 'AdminList', 'AdminAdd']
const nonSuperAdminRoute = ['ReportHome']

router.beforeEach((to, from, next) => {
  window.document.title = to.meta && to.meta.title ? to.meta.title : 'TThai Dashboard'

  const routeName = to.name

  const role = getRole()

  if (publicRoute.some((pr) => pr === routeName)) {
    next()
  } else {
    const token = getAccessToken()

    if (!token) {
      router.replace({ name: 'SignOutPage' })
    } else {
      if (superAdminRoute.some((sar) => sar === routeName) && role !== 'super-admin') {
        router.replace({ name: 'SignOutPage' })
      }

      if (nonSuperAdminRoute.some((nsar) => nsar === routeName) && role === 'super-admin') {
        router.replace({ name: 'ClientList' })
      }
      next()
    }
  }
  return next()
})

export default router
