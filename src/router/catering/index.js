export default {
  path: '/catering',
  component: () => import('@/views/catering/index.vue'),
  children: [
    {
      path: '',
      name: 'CateringPage',
      component: () => import('@/views/catering/page/CateringPage.vue'),
      meta: {
        appBar: {
          title: 'Catering',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-food-drumstick' }
        }
      }
    }

  ]
}
