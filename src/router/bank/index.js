export default {
  path: '/bank',
  component: () => import('@/views/bank/index.vue'),
  children: [
    {
      path: 'setting',
      name: 'BankSetting',
      component: () => import('@/views/bank/pages/BankSetting.vue'),
      meta: {
        appBar: {
          title: 'Bank Account',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-bank' }
        }
      }
    }

  ]
}
