export default {
  path: '/website',
  component: () => import('@/views/web-setting/index.vue'),
  children: [
    {
      path: '',
      name: 'WebSettingList',
      component: () => import('@/views/web-setting/page/WebSetting.vue'),
      meta: {
        appBar: {
          title: 'Webstie Setting',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-web' }
        }
      }
    }

  ]
}
