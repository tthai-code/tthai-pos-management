export default {
  path: '/signup',
  component: () => import('@/views/register/index.vue'),
  children: [
    {
      path: '',
      name: 'RegisterPage',
      component: () => import('@/views/register/pages/RegisterPage.vue'),
      meta: {
        title: 'TThai Sign Up',
        layout: 'blank'
      }
    }

  ]
}
