export default {
  path: '/receipt',
  component: () => import('@/views/receipt/index.vue'),
  children: [
    {
      path: '',
      name: 'ReceiptPage',
      component: () => import('@/views/receipt/page/ReceiptPage.vue'),
      meta: {
        appBar: {
          title: 'Receipt',
          // FIXME: mdi-account-circle-outline
          icon: { icon: 'mdi-receipt-text-outline' }
        }
      }
    }

  ]
}
