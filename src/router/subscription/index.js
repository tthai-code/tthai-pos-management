export default {
  path: '/subscription',
  component: () => import('@/views/subscription/index.vue'),
  children: [
    {
      path: '',
      name: 'subscriptionList',
      component: () => import('@/views/subscription/pages/SubscriptionList.vue'),
      meta: {
        appBar: {
          title: 'Subscription',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-pin' }
        }
      }
    },
    {
      path: 'add',
      name: 'subscriptionAdd',
      component: () => import('@/views/subscription/pages/SubscriptionAdd.vue'),
      meta: {
        appBar: {
          title: 'Subscription',
          // FIXME: mdi-stool
          icon: { icon: 'mdi-pin' }
        }
      }
    }
  ]
}
