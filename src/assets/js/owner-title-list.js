const OwnerTitleEnum = [
  {
    text: 'Partner',
    value: 'PARTNER'
  },
  {
    text: 'Owner',
    value: 'OWNER'
  },
  {
    text: 'President',
    value: 'PRESIDENT'
  },
  {
    text: 'Vice President',
    value: 'VICE_PRESIDENT'
  },
  {
    text: 'Member LLC',
    value: 'MEMBER_LLC'
  },
  {
    text: 'Secretary',
    value: 'SECRETARY'
  },
  {
    text: 'Treasurer',
    value: 'TREASURER'
  },
  {
    text: 'CEO',
    value: 'CEO'
  },
  {
    text: 'CFO',
    value: 'CFO'
  },
  {
    text: 'COO',
    value: 'COO'
  }
]

export default OwnerTitleEnum
