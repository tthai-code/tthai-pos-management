import Vue from 'vue'
import Vuex from 'vuex'
import { vuexUser } from '@/plugins/VuexPersist'

import UserModule from './user'
import RestaurantModule from './restaurant'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    User: UserModule,
    Restaurant: RestaurantModule
  },
  plugins: [
    vuexUser.plugin
  ]
})

export default store
