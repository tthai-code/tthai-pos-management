const state = {
  bankAccount: false,
  paymentGateway: false,
  subscription: []
}

const actions = {
  setAvailable ({ commit }, payload) {
    commit('SET_AVAILABLE', payload)
  },
  resetAvailable ({ commit }) {
    commit('RESET_AVAILABLE')
  }
}

const mutations = {
  SET_AVAILABLE (state, payload) {
    state.bankAccount = payload?.bankAccount || false
    state.paymentGateway = payload?.paymentGateway || false
    state.subscription = payload?.subscription || []
  },
  RESET_AVAILABLE (state) {
    state.bankAccount = false
    state.paymentGateway = false
    state.subscription = []
  }
}

const getters = {
  isBankAccount: (state) => state.bankAccount,
  isPaymentGateway: (state) => state.paymentGateway,
  isWebsite: (state) => state.subscription.includes('WEBSITE'),
  isCatering: (state) => state.subscription.includes('CATERING'),
  isDelivery: (state) => state.subscription.includes('DELIVERY'),
  isReservation: (state) => state.subscription.includes('RESERVATION'),
  isAccounting: (state) => state.subscription.includes('ACCOUNTING'),
  availableFeature: (state) => ({
    isBankAccount: state.bankAccount,
    isPaymentGateway: state.paymentGateway,
    isWebsite: state.subscription.includes('WEBSITE'),
    isCatering: state.subscription.includes('CATERING'),
    isDelivery: state.subscription.includes('DELIVERY'),
    isReservation: state.subscription.includes('RESERVATION'),
    isAccounting: state.subscription.includes('ACCOUNTING')
  })
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
