import AuthProvider from '@/resources/AuthProvider'
// import RestaurantProvider from '@/resources/Restaurant'
import Base64 from '@/plugins/Base64'

const authService = new AuthProvider()
// const restaurantService = new RestaurantProvider()

const state = {
  accessToken: null,
  id: null,
  username: null,
  tokenExpire: null,
  restaurant: {
    email: null,
    id: null,
    name: null,
    tel: null,
    address: null,
    timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone
  },
  role: null,
  remember: {
    username: null,
    password: null
  }
}

const actions = {
  async signin ({ dispatch }, payload) {
    const res = await authService.login({
      username: payload.username,
      password: payload.password
    })
    if (res.accessToken) {
      dispatch('setUser', {
        ...res.data,
        accessToken: res.accessToken
      })
      if (payload.isRemember) {
        dispatch('setRemember', {
          username: payload.username,
          password: payload.password
        })
      } else {
        dispatch('resetRemember')
      }
    }
  },
  async registerRestaurant ({ dispatch }, payload) {
    dispatch('setUser', payload)
  },
  setUser ({ commit }, payload) {
    commit('SET_USER', payload)
  },
  setRemember ({ commit }, payload) {
    commit('SET_REMEMBER', {
      username: payload.username,
      password: Base64.encodeByKey(payload.password, 'P@s$w0rd')
    })
  },
  resetRemember ({ commit }) {
    commit('RESET_REMEMBER')
  },
  resetUser ({ commit }) {
    commit('RESET_USER')
  },
  async directAdmin ({ dispatch }, payload) {
    const res = await authService.directAdmin(
      payload.id
    )
    if (res.accessToken) {
      dispatch('setUser', {
        ...res.data,
        accessToken: res.accessToken
      })
      if (payload.isRemember) {
        dispatch('setRemember', {
          username: payload.username,
          password: payload.password
        })
      } else {
        dispatch('resetRemember')
      }
    }
  },
  setTimeZone ({ commit }, payload) {
    commit('SET_TIMEZONE', payload)
  }
}

const mutations = {
  SET_USER (state, payload) {
    state.accessToken = payload?.accessToken || null
    state.id = payload?.id || null
    state.username = payload?.username || null
    state.tokenExpire = payload?.tokenExpire || null
    state.restaurant.email = payload?.restaurant?.email || null
    state.restaurant.id = payload?.restaurant?.id || null
    state.restaurant.name = payload?.restaurant?.legalBusinessName || null
    state.restaurant.tel = payload?.restaurant?.businessPhone || null
    state.restaurant.address = payload?.restaurant?.address || null
    state.restaurant.timeZone = payload?.restaurant?.timeZone || Intl.DateTimeFormat().resolvedOptions().timeZone
  },
  SET_REMEMBER (state, payload) {
    state.remember.username = payload?.username || null
    state.remember.password = payload?.password || null
  },
  RESET_USER (state) {
    state.accessToken = null
    state.id = null
    state.username = null
    state.tokenExpire = null
    state.restaurant.email = null
    state.restaurant.id = null
    state.restaurant.name = null
    state.restaurant.tel = null
    state.restaurant.address = null
    state.restaurant.timeZone = null
  },
  RESET_REMEMBER (state) {
    state.remember.username = null
    state.remember.password = null
  },
  SET_TIMEZONE (state, payload) {
    state.restaurant.timeZone = payload || Intl.DateTimeFormat().resolvedOptions().timeZone
  }
}

const getters = {
  restaurantData: (state) => state.restaurant,
  restaurantId: (state) => state.restaurant.id,
  userId: (state) => state.id,
  accessToken: (state) => state.accessToken,
  tokenExpire: (state) => state.tokenExpire,
  role: (state) => state.role,
  username: (state) => state.username,
  remember: (state) => {
    const { remember } = state
    if (remember.username && remember.password) {
      return {
        username: remember.username,
        password: Base64.decodeByKey(remember.password, 'P@s$w0rd')
      }
    }
    return {}
  },
  timeZone: (state) => state.restaurant.timeZone
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
