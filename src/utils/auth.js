import store from '@/store'

export const getAccessToken = () => {
  const tokenExpire = store.getters['User/tokenExpire']
  const accessToken = store.getters['User/accessToken']
  const dateTime = new Date().getTime()
  if (!accessToken || !tokenExpire || tokenExpire <= dateTime) {
    store.dispatch('User/resetUser')
    return null
  }
  return accessToken
}

export const getRole = () => {
  const role = store.getters['User/role']

  return role
}

export const getAuthToken = () => {
  let token = {}
  const accessToken = getAccessToken()
  if (accessToken) {
    token = {
      key: 'Authorization',
      value: `Bearer ${accessToken}`
    }
  }
  return token
}

export const getUserId = () => {
  const userId = store.getters['User/userId']

  return userId
}

export const getRestaurantId = () => {
  const restaurantId = store.getters['User/restaurantId']
  // const role = store.getters['User/role']

  // if (!restaurantId && role !== 'super-admin') {
  //   store.dispatch('User/resetUser')
  //   return null
  // }
  return restaurantId
}

export const getRestaurantData = () => {
  const restaurantData = store.getters['User/restaurantData']
  // const role = store.getters['User/role']
  // if (!restaurantData && role !== 'super-admin') {
  //   store.dispatch('User/resetUser')
  //   return null
  // }
  return restaurantData
}

export default {
  getAccessToken,
  getAuthToken,
  getRestaurantId,
  getRestaurantData,
  getUserId
}
