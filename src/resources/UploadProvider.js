import axios from 'axios'
import { getAuthToken } from '../utils/auth'

class UploadProvider {
  // eslint-disable-next-line class-methods-use-this
  async uploadFile (file) {
    const authData = getAuthToken()
    const formData = new FormData()
    formData.append('file', file)

    const { data } = await axios.post(`${process.env.VUE_APP_API}/uploads`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        [authData.key]: authData.value
      }
    })

    return data
  }
}

export default UploadProvider
