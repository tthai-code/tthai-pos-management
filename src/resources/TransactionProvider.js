import { getAuthToken } from '../utils/auth'
import HttpRequest from './HttpRequest'

class TransactionProvider extends HttpRequest {
  getTransactions (query) {
    this.setHeader(getAuthToken())
    return this.get('/v1/transaction', query)
  }

  RefundTransaction (payload) {
    this.setHeader(getAuthToken())
    return this.put('/v1/transaction/refund', payload)
  }

  closeBatchPayment (payload) {
    this.setHeader(getAuthToken())
    return this.put('/v1/transaction/close-batch', payload)
  }

  getCloseBatchSummary (query) {
    this.setHeader(getAuthToken())
    return this.get('/v1/transaction/close-batch/summary', query)
  }

  voidTransaction (payload) {
    this.setHeader(getAuthToken())
    return this.put('/v1/transaction/void', payload)
  }
}

export default TransactionProvider
