// import { getAuthToken } from '../utils/auth'
import HttpRequest from './HttpRequest'

class PublicAccountProvider extends HttpRequest {
  createPublicAccount (payload) {
    return this.post('/public/account/register', payload)
  }
}

export default PublicAccountProvider
