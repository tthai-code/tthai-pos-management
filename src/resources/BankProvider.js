import { getAuthToken } from '../utils/auth'
import HttpRequest from './HttpRequest'

class BankProvider extends HttpRequest {
  getBankAccount () {
    this.setHeader(getAuthToken())

    return this.get('v1/bank-account')
  }

  createNewBankAccount (payload) {
    this.setHeader(getAuthToken())

    return this.put('v1/bank-account', payload)
  }
}

export default BankProvider
