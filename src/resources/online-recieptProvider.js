import { getAuthToken, getRestaurantId } from '../utils/auth'
import HttpRequest from './HttpRequest'

class OnlineReceiptProvider extends HttpRequest {
  updateOnlineReceipt (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.put(`/v1/online-receipt/${restaurantId}`, payload)
  }

  getOnlineReceipt () {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.get(`/v1/online-receipt/${restaurantId}`)
  }
}

export default OnlineReceiptProvider
