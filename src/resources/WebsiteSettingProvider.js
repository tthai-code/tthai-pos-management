import { getAuthToken, getRestaurantId } from '../utils/auth'
import HttpRequest from './HttpRequest'

class WebsiteSettingProvider extends HttpRequest {
  getWebsiteSetting () {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.get(`v1/web-setting/${restaurantId}`)
  }

  updateWebsiteSetting (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.put(`v1/web-setting/${restaurantId}`, payload)
  }
}

export default WebsiteSettingProvider
