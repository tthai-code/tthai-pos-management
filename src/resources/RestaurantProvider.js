// eslint-disable-next-line import/no-cycle
import { getAuthToken, getUserId, getRestaurantId } from '../utils/auth'

import HttpRequest from './HttpRequest'

class RestaurantProvider extends HttpRequest {
  createRestaurantProfile (payload) {
    this.setHeader(getAuthToken())
    const accountId = getUserId()
    return this.post('/v1/restaurant', { ...payload, accountId })
  }

  getRestaurantById () {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.get(`/v1/restaurant/${restaurantId}`)
  }

  updateRestaurant (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.put(`/v1/restaurant/${restaurantId}`, payload)
  }

  getRestaurantOpenHours () {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.get(`/v1/restaurant/${restaurantId}/hours`)
  }

  getRestaurantShifts () {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.get(`/v1/restaurant/${restaurantId}/shift`)
  }

  updateRestaurantShifts (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.patch(`/v1/restaurant/${restaurantId}/shift`, payload)
  }

  updateRestaurantOpenHours (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.patch(`/v1/restaurant/${restaurantId}/hours`, payload)
  }

  getRestaurantHolidays () {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.get(`/v1/restaurant/${restaurantId}/holiday`)
  }

  updateRestaurantHolidays (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.patch(`/v1/restaurant/${restaurantId}/holiday`, payload)
  }

  getInfoAvailable () {
    this.setHeader(getAuthToken())
    return this.get('v1/restaurant/info/available')
  }
}

export default RestaurantProvider
