import { getAuthToken, getRestaurantId } from '../utils/auth'
import HttpRequest from './HttpRequest'

class TaxProvider extends HttpRequest {
  updateTax (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.patch(`/v1/restaurant/${restaurantId}/tax`, payload)
  }

  getTax () {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.get(`/v1/restaurant/${restaurantId}/tax`)
  }
}

export default TaxProvider
