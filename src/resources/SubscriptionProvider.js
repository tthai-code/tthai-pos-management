import { getAuthToken } from '../utils/auth'
import HttpRequest from './HttpRequest'

class SubscriptionProvider extends HttpRequest {
  getSubscription (query) {
    this.setHeader(getAuthToken())
    return this.get('/v1/subscription', { ...query })
  }

  // getSubscriptionById (id) {
  //   this.setHeader(getAuthToken())
  //   return this.get(`/v1/subscription/${id}`)
  // }

  createSubscription (payload) {
    this.setHeader(getAuthToken())
    // const restaurantId = getRestaurantId()
    return this.post('/v1/subscription', { ...payload })
  }

  // updateSubscription (id, payload) {
  //   this.setHeader(getAuthToken())
  //   return this.put(`/v1/subscription/${id}`, payload)
  // }

  // deleteSubscription (id) {
  //   this.setHeader(getAuthToken())
  //   return this.delete(`/v1/subscription/${id}`)
  // }
}

export default new SubscriptionProvider()
