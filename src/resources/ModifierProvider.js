import { getAuthToken, getRestaurantId } from '../utils/auth'
import HttpRequest from './HttpRequest'

class ModifierProvider extends HttpRequest {
  getModifier (query) {
    this.setHeader(getAuthToken())
    return this.get('/v1/modifier', query)
  }

  getModifierById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/v1/modifier/${id}/modifier`)
  }

  createModifier (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.post('/v1/modifier', { ...payload, restaurantId })
  }

  updateModifier (id, payload) {
    this.setHeader(getAuthToken())
    return this.put(`/v1/modifier/${id}`, payload)
  }

  deleteModifier (id) {
    this.setHeader(getAuthToken())
    return this.delete(`v1/modifier/${id}`)
  }
}

export default ModifierProvider
