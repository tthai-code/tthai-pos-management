import { getAuthToken } from '../utils/auth'
import HttpRequest from './HttpRequest'

class OtherDeviceProvider extends HttpRequest {
  getOtherDevice () {
    this.setHeader(getAuthToken())
    return this.get('v1/other-device')
  }
}

export default OtherDeviceProvider
