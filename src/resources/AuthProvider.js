import HttpRequest from './HttpRequest'

class AuthProvider extends HttpRequest {
  login (payload) {
    return this.post('/public/account/login', payload)
  }

  directAdmin (id) {
    return this.get(`/public/account/login/${id}`)
  }
}

export default AuthProvider
