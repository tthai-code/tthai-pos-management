import { getAuthToken, getRestaurantId } from '../utils/auth'
import HttpRequest from './HttpRequest'

class TableZoneProvider extends HttpRequest {
  getTableZone (query) {
    this.setHeader(getAuthToken())
    return this.get('/v1/table-zone', query)
  }

  getTableZoneById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/v1/table-zone/${id}/table-zone`)
  }

  createTableZone (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.post('/v1/table-zone', { ...payload, restaurantId })
  }

  updateTableZone (id, payload) {
    this.setHeader(getAuthToken())
    return this.put(`/v1/table-zone/${id}`, payload)
  }

  deleteTableZone (id) {
    this.setHeader(getAuthToken())
    return this.delete(`/v1/table-zone/${id}`)
  }
}

export default TableZoneProvider
