import { getAuthToken } from '../utils/auth'
import HttpRequest from './HttpRequest'

class UserProvider extends HttpRequest {
  getMembers (query) {
    this.setHeader(getAuthToken())
    return this.get('/member', { ...query })
  }

  getMemberById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/member/${id}`)
  }

  createMember (payload) {
    this.setHeader(getAuthToken())
    return this.post('/member', { ...payload })
  }

  updateMember (id, payload) {
    this.setHeader(getAuthToken())
    return this.put(`/member/${id}`, payload)
  }

  deleteMember (id) {
    this.setHeader(getAuthToken())
    return this.delete(`/member?id=${id}`)
  }
}

export default UserProvider
