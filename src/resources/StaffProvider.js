import { getAuthToken, getRestaurantId } from '../utils/auth'
import HttpRequest from './HttpRequest'

class UserProvider extends HttpRequest {
  getStaff (query) {
    this.setHeader(getAuthToken())
    return this.get('/v1/staff', { ...query })
  }

  getStaffById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/v1/staff/${id}`)
  }

  createStaff (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.post('/v1/staff', { ...payload, restaurantId })
  }

  updateStaff (id, payload) {
    this.setHeader(getAuthToken())
    return this.put(`/v1/staff/${id}`, payload)
  }

  deleteStaff (id) {
    this.setHeader(getAuthToken())
    return this.delete(`/v1/staff/${id}`)
  }
}

export default UserProvider
