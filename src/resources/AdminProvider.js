import { getAuthToken } from '../utils/auth'
import HttpRequest from './HttpRequest'

class AdminProvider extends HttpRequest {
  getAdmins (query) {
    this.setHeader(getAuthToken())
    return this.get('/users', query)
  }

  getAdminById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/users/${id}`)
  }

  createAdmin (payload) {
    this.setHeader(getAuthToken())
    return this.post('/users', payload)
  }

  updateAdmin (id, payload) {
    this.setHeader(getAuthToken())
    return this.put(`/users/${id}`, payload)
  }

  deleteAdmin (id) {
    this.setHeader(getAuthToken())
    return this.delete(`/users?id=${id}`)
  }
}

export default AdminProvider
