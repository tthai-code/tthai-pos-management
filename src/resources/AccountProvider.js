import { getAuthToken, getUserId } from '../utils/auth'
import HttpRequest from './HttpRequest'

class AccountProvider extends HttpRequest {
  updatePassword (payload) {
    this.setHeader(getAuthToken())
    const userId = getUserId()
    return this.patch(`v1/account/${userId}/password`, payload)
  }

  updateUsername (payload) {
    this.setHeader(getAuthToken())
    const userId = getUserId()
    return this.patch(`v1/account/${userId}/username`, payload)
  }
}

export default AccountProvider
