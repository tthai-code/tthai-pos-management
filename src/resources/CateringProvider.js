import { getAuthToken, getRestaurantId } from '../utils/auth'
import HttpRequest from './HttpRequest'

class CateringProvider extends HttpRequest {
  getCatering () {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.get(`v1/catering/${restaurantId}`)
  }

  updateCatering (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.put(`v1/catering/${restaurantId}`, payload)
  }
}

export default CateringProvider
