import { getAuthToken, getRestaurantId } from '../utils/auth'
import HttpRequest from './HttpRequest'

class MenuProvider extends HttpRequest {
  getMenu (query) {
    this.setHeader(getAuthToken())
    return this.get('/v1/menu', query)
  }

  getMenuCategory (query) {
    this.setHeader(getAuthToken())
    return this.get('/v1/menu-category', query)
  }

  getMenuCategoryHour (query) {
    this.setHeader(getAuthToken())
    return this.get('/v1/menu-category/hours', query)
  }

  getMenuModifierList (id) {
    this.setHeader(getAuthToken())
    return this.get(`/v1/menu-modifier/${id}`)
  }

  getMenuById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/v1/menu/${id}`)
  }

  getMenuCategoryById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/v1/menu-category/${id}/category`)
  }

  getMenuTypeById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/menu-categories/${id}`)
  }

  createMenu (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.post('/v1/menu', { ...payload, restaurantId })
  }

  createMenuCategory (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.post('/v1/menu-category', { ...payload, restaurantId })
  }

  updateMenu (id, payload) {
    this.setHeader(getAuthToken())
    return this.put(`/v1/menu/${id}`, payload)
  }

  updateMenuCategory (id, payload) {
    this.setHeader(getAuthToken())
    return this.put(`v1/menu-category/${id}`, payload)
  }

  deleteMenu (id) {
    this.setHeader(getAuthToken())
    return this.delete(`/v1/menu/${id}`)
  }

  deleteMenuCategory (id) {
    this.setHeader(getAuthToken())
    return this.delete(`v1/menu-category/${id}`)
  }

  updateMenuPosition (payload) {
    this.setHeader(getAuthToken())
    return this.put('v1/menu/position/all', payload)
  }

  updateCategoryPosition (payload) {
    this.setHeader(getAuthToken())
    return this.put('v1/menu-category/position/all', payload)
  }
}

export default MenuProvider
