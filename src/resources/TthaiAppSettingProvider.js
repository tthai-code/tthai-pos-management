import { getAuthToken, getRestaurantId } from '../utils/auth'
import HttpRequest from './HttpRequest'

class TthaiAppSettingProvider extends HttpRequest {
  getTthaiAppSetting () {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.get(`v1/tthai-app/${restaurantId}`)
  }

  updateIsMobileOrdering (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.patch(`v1/tthai-app/${restaurantId}/mobile-ordering`, payload)
  }

  updatePickupHours (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.patch(`v1/tthai-app/${restaurantId}/pickup-hours`, payload)
  }

  updateWaitTime (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.patch(`v1/tthai-app/${restaurantId}/wait-time`, payload)
  }

  updateItemSpecialInstructions (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.patch(`v1/tthai-app/${restaurantId}/item-special`, payload)
  }

  updateCustomizeAppText (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.patch(`v1/tthai-app/${restaurantId}/in-app-text`, payload)
  }

  updateTthaiAppSetting (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.put(`v1/tthai-app/${restaurantId}`, payload)
  }
}

export default TthaiAppSettingProvider
