import { getAuthToken, getRestaurantId } from '../utils/auth'
import HttpRequest from './HttpRequest'

class TableProvider extends HttpRequest {
  getTable (query) {
    this.setHeader(getAuthToken())
    return this.get('/v1/table', query)
  }

  getTableById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/v1/table/${id}/table`)
  }

  createTable (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.post('/v1/table', { ...payload, restaurantId })
  }

  updateTable (id, payload) {
    this.setHeader(getAuthToken())
    return this.put(`/v1/table/${id}`, payload)
  }

  deleteTable (id) {
    this.setHeader(getAuthToken())
    return this.delete(`/v1/table/${id}`)
  }
}

export default TableProvider
