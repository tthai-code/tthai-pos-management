import { getAuthToken, getRestaurantId } from '../utils/auth'
import HttpRequest from './HttpRequest'

class DeviceProvider extends HttpRequest {
  getDevice () {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.get(`v1/device/${restaurantId}/restaurant`)
  }

  getLimitDevice () {
    this.setHeader(getAuthToken())
    return this.get('v1/device/limit/capacity')
  }

  createDevice (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.post('v1/device', { ...payload, restaurantId })
  }

  deleteDevice (id) {
    this.setHeader(getAuthToken())

    return this.delete(`v1/device/${id}`)
  }

  updateDevice (id, payload) {
    this.setHeader(getAuthToken())

    return this.put(`v1/device/${id}`, payload)
  }
}

export default DeviceProvider
