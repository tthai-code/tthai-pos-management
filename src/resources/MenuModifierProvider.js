import { getAuthToken } from '../utils/auth'
import HttpRequest from './HttpRequest'

class MenuModifierProvider extends HttpRequest {
  getMenuModifier (query) {
    this.setHeader(getAuthToken())
    return this.get('/v1/menu-modifier', query)
  }

  getMenuModifierById (id) {
    this.setHeader(getAuthToken())
    return this.get(`/v1/menu-modifier/${id}/modifier`)
  }

  createMenuModifier (payload) {
    this.setHeader(getAuthToken())

    return this.post('/v1/menu-modifier', { ...payload })
  }

  updateMenuModifier (id, payload) {
    this.setHeader(getAuthToken())
    return this.put(`/v1/menu-modifier/${id}`, payload)
  }

  deleteMenuModifier (id) {
    this.setHeader(getAuthToken())
    return this.delete(`/v1/menu-modifier/${id}`)
  }

  addBulkMenuModifiers (menuId, payload) {
    this.setHeader(getAuthToken())
    return this.post(`v1/menu-modifier/${menuId}/add-bulk`, payload)
  }

  deleteBulkMenuModifier (menuId, payload) {
    this.setHeader(getAuthToken())
    return this.put(`v1/menu-modifier/${menuId}/delete-bulk`, payload)
  }
}

export default MenuModifierProvider
