import { getAuthToken } from '../utils/auth'
import HttpRequest from './HttpRequest'

class CashDrawerProvider extends HttpRequest {
  getCashDrawers (startDate, endDate) {
    this.setHeader(getAuthToken())

    return this.get(`v1/cash-drawer?startDate=${startDate}&endDate=${endDate}`)
  }

  getCashActivity (id) {
    this.setHeader(getAuthToken())

    return this.get(`v1/cash-drawer/${id}`)
  }
}

export default CashDrawerProvider
