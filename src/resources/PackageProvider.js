import { getAuthToken } from '../utils/auth'
import HttpRequest from './HttpRequest'

class PackageProvider extends HttpRequest {
  getPackage (query) {
    this.setHeader(getAuthToken())
    return this.get('/v1/package', { ...query })
  }

  // getPackageById (id) {
  //   this.setHeader(getAuthToken())
  //   return this.get(`/v1/package/${id}`)
  // }

  // createPackage (payload) {
  //   this.setHeader(getAuthToken())
  //   const restaurantId = getRestaurantId()
  //   return this.post('/v1/package', { ...payload, restaurantId })
  // }
}

export default new PackageProvider()
