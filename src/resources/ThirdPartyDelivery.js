import { getAuthToken, getRestaurantId } from '../utils/auth'
import HttpRequest from './HttpRequest'

class ThirdPartyProvider extends HttpRequest {
  getThirdParty () {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.get(`v1/delivery/${restaurantId}/all`)
  }

  updateThirdParty (id, payload) {
    this.setHeader(getAuthToken())

    return this.put(`v1/delivery/${id}`, payload)
  }

  getUberEatsAuthUrl () {
    this.setHeader(getAuthToken())

    return this.get('v1/delivery/ubereats/auth')
  }

  handleConnection (id, payload) {
    this.setHeader(getAuthToken())

    return this.patch(`v1/delivery/${id}/connect-provider`, payload)
  }
}

export default ThirdPartyProvider
