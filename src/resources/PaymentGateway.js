import { getAuthToken, getRestaurantId } from '../utils/auth'
import HttpRequest from './HttpRequest'

class PaymentGatewayProvider extends HttpRequest {
  getPaymentGateway () {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.get(`v1/payment-gateway/${restaurantId}`)
  }

  updatePaymentGateway (payload) {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.put(`v1/payment-gateway/${restaurantId}`, payload)
  }

  createMerchantCoPilot (payload) {
    this.setHeader(getAuthToken())
    return this.post('v1/payment-gateway/merchant', payload)
  }

  getMerchantStatus () {
    this.setHeader(getAuthToken())
    const restaurantId = getRestaurantId()
    return this.get(`v1/payment-gateway/${restaurantId}/merchant`)
  }
}

export default PaymentGatewayProvider
