import Vue from 'vue'

export default class ToDate {
  constructor () {
    this.vue = Vue
  }

  // eslint-disable-next-line class-methods-use-this
  separate (val) {
    const str = val.toString().split('.')
    str[0] = str[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',')
    return str.join('.')
  }

  // eslint-disable-next-line class-methods-use-this
  localeString (val) {
    return val.toLocaleString('en-US', { style: 'currency', currency: 'USD' })
  }
}
