import Vue from 'vue'

Vue.filter('paymentMethodFilter', (val) => {
  switch (val) {
  case 'CASH':
    return 'Cash'
  case 'CREDIT':
    return 'Credit'
  case 'DEBIT':
    return 'Debit'
  case 'THIRD_PARTY':
    return 'Third Party'
  case 'GIFT_CARD':
    return 'Gift Card'
  case 'CHECK':
    return 'Check'
  case 'DELIVERY':
    return 'Delivery'
  case 'DE_MINIMIS':
    return 'Fringe Benefit'
  case 'OTHER':
    return 'Others'
  default:
    return val
  }
})
