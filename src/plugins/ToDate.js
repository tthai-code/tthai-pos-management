import Vue from 'vue'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import timezone from 'dayjs/plugin/timezone'

dayjs.extend(utc)
dayjs.extend(timezone)

Vue.prototype.$dayjs = dayjs

export default class ToDate {
  constructor () {
    this.vue = Vue
  }

  // eslint-disable-next-line class-methods-use-this
  stdTime (time) {
    if (!time) return '-'
    const mokTime = dayjs(new Date()).format(`YYYY-MM-DDT${time}:ss`)

    return dayjs(mokTime).format('hh:mm A')
  }

  // eslint-disable-next-line class-methods-use-this
  ddddmm (val) {
    if (!val) return '-'

    const date = dayjs(val).format('D MMM')

    const dayIndex = dayjs(val).get('day')

    switch (dayIndex) {
    case 0:
      return `Sunday, ${date}`

    case 1:
      return `Monday, ${date}`

    case 2:
      return `Tuesday, ${date}`

    case 3:
      return `Wednesday, ${date}`

    case 4:
      return `Thursday, ${date}`

    case 5:
      return `Friday, ${date}`

    case 6:
      return `Saturday, ${date}`

    default:
      return '-'
    }
  }

  // eslint-disable-next-line class-methods-use-this
  ddddmmmmdd (val) {
    if (!val) return '-'

    const date = dayjs(val).format('MMMM DD')

    const dayIndex = dayjs(val).get('day')

    switch (dayIndex) {
    case 0:
      return `Sunday, ${date}`

    case 1:
      return `Monday, ${date}`

    case 2:
      return `Tuesday, ${date}`

    case 3:
      return `Wednesday, ${date}`

    case 4:
      return `Thursday, ${date}`

    case 5:
      return `Friday, ${date}`

    case 6:
      return `Saturday, ${date}`

    default:
      return '-'
    }
  }

  // eslint-disable-next-line class-methods-use-this
  dddmmddyyyy (val) {
    if (!val) return '-'

    const now = dayjs(val)

    const date = now.format('MMMM D, YYYY')

    const dayIndex = now.get('day')

    switch (dayIndex) {
    case 0:
      return `Sunday, ${date}`

    case 1:
      return `Monday, ${date}`

    case 2:
      return `Tuesday, ${date}`

    case 3:
      return `Wednesday, ${date}`

    case 4:
      return `Thursday, ${date}`

    case 5:
      return `Friday, ${date}`

    case 6:
      return `Saturday, ${date}`

    default:
      return '-'
    }
  }

  // eslint-disable-next-line class-methods-use-this
  stdDate (val) {
    if (!val) return '-'

    return dayjs(val).format('M/D/YYYY')
  }

  // eslint-disable-next-line class-methods-use-this
  mmyyyy (val) {
    if (!val) return '-'

    return dayjs(val).format('MMM. YYYY')
  }

  // eslint-disable-next-line class-methods-use-this
  mmddyyyytt (val) {
    if (!val) return '-'

    return dayjs(val).format('MM/DD/YYYY , hh:mm A')
  }

  // eslint-disable-next-line class-methods-use-this
  mmmddtt (val) {
    if (!val) return '-'

    return dayjs(val).format('MMM. DD, hh:mm A')
  }
}
