import Vue from 'vue'
import _ from 'lodash'

export default class Clone {
  constructor () {
    this.vue = Vue
  }

  // eslint-disable-next-line class-methods-use-this
  clone (data) {
    const clonedData = _.cloneDeep(data)

    return clonedData
  }
}
