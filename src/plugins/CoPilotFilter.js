import Vue from 'vue'

Vue.filter('boardingProcessStatusFilter', (val = '') => {
  switch (val) {
  case 'INPROG':
    return 'Not Submitted.'
  case 'OFS':
    return 'Pending Signature.'
  case 'QUALIFY':
    return 'Qualifying.'
  case 'UNDER':
    return 'Underwriting.'
  case 'DECLINED':
    return 'Declined.'
  case 'BOARDING':
    return 'Boarding'
  case 'BOARDED':
    return 'Boarded'
  case 'LIVE':
    return 'Live'
  case 'CANCELLED':
    return 'Cancelled'
  default:
    return '-'
  }
})

Vue.filter('signatureStatusFilter', (val = '') => {
  switch (val) {
  case 'NOT_SENT':
    return 'Not Sent.'
  case 'PENDING':
    return 'Pending.'
  case 'SIGNED':
    return 'Signed.'
  default:
    return '-'
  }
})

Vue.filter('gatewayBoardingStatusFilter', (val = '') => {
  switch (val) {
  case 'NOT_BOARDED':
    return 'Not Boarded.'
  case 'BOARDED':
    return 'Boarded.'
  default:
    return '-'
  }
})
