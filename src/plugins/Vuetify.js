import Vue from 'vue'
import Vuetify from 'vuetify/lib/framework'
import minifyTheme from 'minify-css-string'
import importAll from '@/assets/js/svg-loader'

import '@mdi/font/css/materialdesignicons.css'

Vuetify.config.silent = true

Vue.use(Vuetify)

const customIcon = importAll(
  require.context('@/assets/svg/', true, /\.svg$/),
  'mwbkk'
)

console.log('customIcon', customIcon)

export default new Vuetify({
  icons: {
    iconfont: 'mdi',
    values: { ...importAll(
      require.context('@/assets/svg/', true, /\.svg$/),
      'mwbkk'
    ) }
  },
  theme: {
    themes: {
      light: {
        primary: '#B2CC53',
        secondary: '#333333',
        accent: '#8c9eff',
        error: '#DC5151',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFEE58'
      }
    },
    options: {
      customProperties: true,
      minifyTheme
    }
  }
})
