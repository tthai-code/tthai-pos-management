import Vue from 'vue'

function formatTime (timeString) {
  const [hourString, minute] = timeString.split(':')
  const hour = +hourString % 24
  return `${(hour % 12 || 12).toString().padStart(2, '0')}:${ minute }${hour < 12 ? 'AM' : 'PM'}`
}

Vue.prototype.$formatTime = formatTime
