const ownerDisplay = (text) => {
  switch (text) {
  case 'PARTNER':
    return 'Partner'
  case 'OWNER':
    return 'Owner'
  case 'PRESIDENT':
    return 'President'
  case 'VICE_PRESIDENT':
    return 'Vice President'
  case 'MEMBER_LLC':
    return 'Member LLC'
  case 'SECRETARY':
    return 'Secretary'
  case 'TREASURER':
    return 'Treasurer'
  case 'CEO':
    return 'CEO'
  case 'CFO':
    return 'CFO'
  case 'COO':
    return 'COO'
  default:
    return ''
  }
}

export default ownerDisplay
