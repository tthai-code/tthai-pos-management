import Vue from 'vue'

Vue.filter('cashAction', (item) => {
  if (!item) return '-'

  switch (item) {
  case 'STARTING_CASH':
    return 'Starting Cash'

  case 'CASH_IN':
    return 'Cash In'

  case 'CASH_OUT':
    return 'Cash Out'

  case 'CASH_COUNT':
    return 'Cash Count'

  case 'CASH_SALES':
    return 'Cash Sales'

  default:
    return '-'
  }
})
