import Vue from 'vue'
import App from '@/App.vue'
import router from '@/router'
import store from '@/store'

import Vuetify from '@/plugins/Vuetify'
import Notification from '@/services/notification'
import ToDate from '@/plugins/ToDate'
import Clone from '@/plugins/Clone'
import Seperator from '@/plugins/Seperator'
import '@/plugins/PhoneFilter'
import '@/plugins/CashDrawerFilter'
import '@/plugins/CoPilotFilter'
import '@/plugins/TimeConverter'
import '@/plugins/TransactionFilter'

import '@/assets/scss/main.scss'
import '@/plugins/GlobalComponents'

Vue.config.productionTip = false
Vue.prototype.$notify = new Notification()
Vue.prototype.$todate = new ToDate()
Vue.prototype.$clone = new Clone()
Vue.prototype.$seperator = new Seperator()

new Vue({
  router,
  store,
  vuetify: Vuetify,
  render: (h) => h(App)
}).$mount('#app')
